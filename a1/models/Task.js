let mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Task Name Required"]
    },
    status: {
        type: String,
        default: "pending"
    }
});

const TaskModel = mongoose.model("Task", taskSchema);

module.exports = TaskModel;
