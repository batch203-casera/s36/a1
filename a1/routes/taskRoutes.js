const taskControllers = require('../controllers/taskControllers');
const express = require('express');

const router = express.Router(); // Allows access to http methods

console.log(taskControllers);

router.post('/create', taskControllers.createTask);
router.get('/getAll', taskControllers.getTasks);
router.patch('/updateTask/:id', taskControllers.updateTask);
router.delete('/deleteTask/:id', taskControllers.deleteTask);

module.exports = router;