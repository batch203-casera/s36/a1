const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');

router.get('/getAllUsers', userController.getAllUsers);
router.patch('/updateUser/:userId', userController.updateUserPassword);

module.exports = router;