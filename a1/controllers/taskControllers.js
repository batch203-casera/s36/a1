const TaskModel = require('../models/Task');

module.exports.createTask = (req, res) => {
    TaskModel.findOne({ name: req.body.name }, (err, result) => {
        if (result === null && result !== req.body.name) {
            let newTask = new TaskModel({
                name: req.body.name
            });
            newTask.save((saveErr, savedTask) => {
                if (saveErr) return res.send(saveErr);
                else return res.status(200).send("Added Task");
            })
        }
        else {
            return res.send("Task with the same name was already created");
        }
    });
}

module.exports.getTasks = (req, res) => {
    TaskModel.find({}, (err, result) => {
        res.send(result);
    })
}

module.exports.updateTask = (req, res) => {
    let id = req.params.id;
    TaskModel.findByIdAndUpdate(id, { status: req.body.status }, { new: true })
        .then(updatedTask => {
            console.log(updatedTask);
            res.send('Updated');
        }
        )
        .catch(err => console.log(err));
}

module.exports.deleteTask = (req, res) => {
    TaskModel.findByIdAndDelete(req.params.id)
        .then(deleted => res.send(deleted))
        .catch(err => console.log(err));
}

