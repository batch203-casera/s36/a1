const UserModel = require('../models/User');

module.exports.getAllUsers = (req, res) => {
    UserModel.find({}, (err, users) => {
        res.send(users);
    });
}

module.exports.updateUserPassword = (req, res) => {
    let id = req.params.userId;
    UserModel.findByIdAndUpdate(id, {
        password: req.body.password
    }, { new: true })
        .then(updatedPassword => res.send(updatedPassword.username))
        .catch(err => console.log(err));

}
